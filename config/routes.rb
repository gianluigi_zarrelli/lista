# frozen_string_literal: true
Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :boards, only: %i[index show create destroy]
      resources :lists, only: %i[create destroy]
      resources :cards, only: %i[create update destroy]
    end
  end

  root to: 'home#index'
  get '*path', to: 'home#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
