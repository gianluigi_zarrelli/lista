# frozen_string_literal: true
class Board < ApplicationRecord
  has_many :lists, dependent: :destroy

  def as_json(*)
    super.tap do |hash|
      hash['lists'] = lists
    end
  end
end
