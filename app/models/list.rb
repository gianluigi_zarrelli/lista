# frozen_string_literal: true
class List < ApplicationRecord
  belongs_to :board
  has_many :cards, dependent: :destroy

  def as_json(*)
    super.tap do |hash|
      hash['cards'] = cards
    end
  end
end
