/* eslint-disable import/no-extraneous-dependencies */
import React, { useEffect, useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {
  Layout, Breadcrumb, Card, Row, Col, PageHeader, Button,
} from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import axios from 'axios';

function Boards(props) {
  const { history } = props;
  const { Content } = Layout;
  const [boards, setBoards] = useState([]);

  const getBoards = useCallback(() => {
    const { CancelToken } = axios;
    const source = CancelToken.source();

    axios.get('/api/v1/boards')
      .then((response) => {
        const { data } = response;
        setBoards(data);
      }).catch((thrown) => {
        if (!axios.isCancel(thrown)) {
          // eslint-disable-next-line no-console
          console.log('Request failed:', thrown.message);
        }
      });

    return source;
  }, []);

  const handleAddBoard = useCallback(() => {
    history.push('/boards/new');
  }, []);

  useEffect(() => {
    const source = getBoards();
    return () => source.cancel();
  }, [getBoards]);

  return (
    <Content style={{ margin: '0 16px' }}>
      <Breadcrumb style={{ margin: '16px 0' }}>
        <Breadcrumb.Item>Boards</Breadcrumb.Item>
      </Breadcrumb>
      <PageHeader
        ghost={false}
        title="Boards"
        subTitle={`Boards: ${boards.length}`}
        extra={[
          <Button key="1" type="primary" onClick={handleAddBoard}>
            <PlusOutlined />
            {' '}
            Board
          </Button>,
        ]}
      />
      <div className="site-layout-background" style={{ padding: 24 }}>
        <Row>
          {boards.map((board) => (
            <Col key={board.id} xs={24} sm={12} md={12} lg={6} xl={6} style={{ padding: 8 }}>
              <Link to={`/boards/${board.id}`}>
                <Card
                  bordered={false}
                  hoverable
                  style={{ height: 100, backgroundColor: 'rgb(0, 121, 191)', color: '#fff' }}
                >
                  <p>{board.name}</p>
                </Card>
              </Link>
            </Col>
          ))}
        </Row>
      </div>
    </Content>
  );
}

Boards.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  history: PropTypes.object.isRequired,
};

export default Boards;
