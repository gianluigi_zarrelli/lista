import React, { useCallback } from 'react';
import { useParams } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  Layout, Breadcrumb, PageHeader, Form, Input, Button,
} from 'antd';
import axios from 'axios';

function AddList(props) {
  const { history } = props;
  const { id } = useParams();
  const { Content } = Layout;
  const handleFinish = useCallback((values) => {
    const token = document.querySelector('meta[name=csrf-token]').content;

    axios.post('/api/v1/lists', {
      list: {
        name: values.name,
        board_id: id,
      },
    }, {
      headers: {
        'X-CSRF-Token': token,
      },
    }).then((response) => {
      const { success } = response.data;
      if (success) {
        history.push(`/boards/${id}`);
      }
    });
  }, []);

  return (
    <Content style={{ margin: '0 16px' }}>
      <Breadcrumb style={{ margin: '16px 0' }}>
        <Breadcrumb.Item>Boards</Breadcrumb.Item>
        <Breadcrumb.Item>{id}</Breadcrumb.Item>
        <Breadcrumb.Item>New</Breadcrumb.Item>
      </Breadcrumb>
      <PageHeader
        ghost={false}
        onBack={() => history.push(`/boards/${id}`)}
        title="New List"
      />
      <div className="site-layout-background" style={{ padding: 24 }}>
        <Form onFinish={handleFinish}>
          <Form.Item
            label="Name"
            name="name"
            rules={[
              {
                required: true,
                message: 'Please input a list name!',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    </Content>
  );
}

AddList.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  history: PropTypes.object.isRequired,
};

export default AddList;
