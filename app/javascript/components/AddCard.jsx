import React, { useCallback } from 'react';
import { useParams } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  Layout, Breadcrumb, PageHeader, Form, Input, Button, Select,
} from 'antd';
import axios from 'axios';

function AddCard(props) {
  const { history } = props;
  const { id, listId } = useParams();
  const { Content } = Layout;
  const { TextArea } = Input;
  const { Option } = Select;

  const handleFinish = useCallback((values) => {
    const token = document.querySelector('meta[name=csrf-token]').content;

    axios.post('/api/v1/cards', {
      card: {
        name: values.name,
        description: values.description,
        prio: values.prio,
        status: values.status,
        list_id: listId,
      },
    }, {
      headers: {
        'X-CSRF-Token': token,
      },
    }).then((response) => {
      const { success } = response.data;
      if (success) {
        history.push(`/boards/${id}`);
      }
    });
  }, []);

  return (
    <Content style={{ margin: '0 16px' }}>
      <Breadcrumb style={{ margin: '16px 0' }}>
        <Breadcrumb.Item>Boards</Breadcrumb.Item>
        <Breadcrumb.Item>{id}</Breadcrumb.Item>
        <Breadcrumb.Item>Lists</Breadcrumb.Item>
        <Breadcrumb.Item>{listId}</Breadcrumb.Item>
        <Breadcrumb.Item>New</Breadcrumb.Item>
      </Breadcrumb>
      <PageHeader
        ghost={false}
        onBack={() => history.push(`/boards/${id}`)}
        title="New Card"
      />
      <div className="site-layout-background" style={{ padding: 24 }}>
        <Form onFinish={handleFinish} layout="vertical">
          <Form.Item
            label="Name"
            name="name"
            rules={[
              {
                required: true,
                message: 'Please input a card name!',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Description"
            name="description"
            rules={[
              {
                required: true,
                message: 'Please input a card description!',
              },
            ]}
          >
            <TextArea rows={4} />
          </Form.Item>

          <Form.Item
            label="Prio"
            name="prio"
            rules={[
              {
                required: true,
                message: 'Please input a card prio!',
              },
            ]}
          >
            <Select>
              <Option value="1">1</Option>
              <Option value="2">2</Option>
              <Option value="3">3</Option>
              <Option value="4">4</Option>
              <Option value="5">5</Option>
            </Select>
          </Form.Item>

          <Form.Item
            label="Status"
            name="status"
            rules={[
              {
                required: true,
                message: 'Please input a card status!',
              },
            ]}
          >
            <Select>
              <Option value="Backlog">Backlog</Option>
              <Option value="ToDo">ToDo</Option>
              <Option value="In Progress">In Progress</Option>
              <Option value="Testing">Testing</Option>
              <Option value="Done">Done</Option>
            </Select>
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    </Content>
  );
}

AddCard.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  history: PropTypes.object.isRequired,
};

export default AddCard;
