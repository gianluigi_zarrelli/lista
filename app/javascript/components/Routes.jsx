import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Board from './Board';
import AddBoard from './AddBoard';
import AddList from './AddList';
import AddCard from './AddCard';
import Boards from './Boards';

function Routes() {
  return (
    <Switch>
      <Route path="/boards/:id/lists/:listId/cards/new" component={AddCard} />
      <Route path="/boards/:id/lists/new" component={AddList} />
      <Route path="/boards/new" component={AddBoard} />
      <Route path="/boards/:id" component={Board} />
      <Route exact path="/" component={Boards} />
    </Switch>
  );
}

export default Routes;
