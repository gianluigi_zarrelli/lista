/* eslint-disable import/no-extraneous-dependencies */
import React, { useState, useCallback } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Layout, Menu } from 'antd';
import { DashboardOutlined } from '@ant-design/icons';
import Routes from './Routes';

function Container(props) {
  const { history } = props;
  const { Footer, Sider } = Layout;
  const [collapsed, setCollapsed] = useState(true);

  const goBack = useCallback(() => {
    history.push('/');
  }, []);

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sider collapsible collapsed={collapsed} onCollapse={setCollapsed}>
        <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
          <Menu.Item key="1" icon={<DashboardOutlined />} onClick={goBack}>Boards</Menu.Item>
        </Menu>
      </Sider>
      <Layout className="site-layout">
        <Routes />
        <Footer style={{ textAlign: 'center' }}>Lista ©2020 Created by Gianluigi Zarrelli</Footer>
      </Layout>
    </Layout>
  );
}

Container.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  history: PropTypes.object.isRequired,
};

export default withRouter(Container);
