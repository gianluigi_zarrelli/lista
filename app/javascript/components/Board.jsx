/* eslint-disable react/jsx-no-bind */
/* eslint-disable import/no-extraneous-dependencies */
import React, { useEffect, useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { useParams } from 'react-router-dom';
import {
  Layout, Breadcrumb, Row, Col, Card, Button, PageHeader, Select,
} from 'antd';
import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import axios from 'axios';

function Board(props) {
  const { history } = props;
  const { Content } = Layout;
  const { id } = useParams();
  const [board, setBoard] = useState(null);
  const { Option } = Select;

  const getBoard = useCallback(() => {
    const { CancelToken } = axios;
    const source = CancelToken.source();

    axios.get(`/api/v1/boards/${id}`)
      .then((response) => {
        const { data } = response;
        setBoard(data);
      }).catch((thrown) => {
        if (!axios.isCancel(thrown)) {
          // eslint-disable-next-line no-console
          console.log('Request failed:', thrown.message);
        }
      });

    return source;
  }, []);

  const handleAddList = useCallback(() => {
    history.push(`/boards/${id}/lists/new`);
  }, []);

  const handleAddCard = useCallback((listId) => {
    history.push(`/boards/${id}/lists/${listId}/cards/new`);
  }, []);

  const handleRemove = useCallback(() => {
    const token = document.querySelector('meta[name=csrf-token]').content;

    axios.delete(`/api/v1/boards/${id}`, {
      headers: {
        'X-CSRF-Token': token,
      },
    }).then((response) => {
      const { success } = response.data;
      if (success) {
        history.push('/');
      }
    });
  }, []);

  const handleRemoveList = useCallback((listId) => {
    const token = document.querySelector('meta[name=csrf-token]').content;

    axios.delete(`/api/v1/lists/${listId}`, {
      headers: {
        'X-CSRF-Token': token,
      },
    }).then((response) => {
      const { success } = response.data;
      if (success) {
        window.location.reload(true);
      }
    });
  }, []);

  const handleRemoveCard = useCallback((cardId) => {
    const token = document.querySelector('meta[name=csrf-token]').content;

    axios.delete(`/api/v1/cards/${cardId}`, {
      headers: {
        'X-CSRF-Token': token,
      },
    }).then((response) => {
      const { success } = response.data;
      if (success) {
        window.location.reload(true);
      }
    });
  }, []);

  const handleSelect = useCallback((cardId) => {
    setTimeout(() => {
      const token = document.querySelector('meta[name=csrf-token]').content;
      const value = document.querySelector(`#card_${cardId}`).parentElement.nextElementSibling.innerText;

      axios.put(`/api/v1/cards/${cardId}`, {
        card: {
          status: value,
        },
      }, {
        headers: {
          'X-CSRF-Token': token,
        },
      }).then((response) => {
        const { success } = response.data;
        if (success) {
          // eslint-disable-next-line no-console
          console.log('Status changed.');
          // window.location.reload(true);
        }
      });
    }, 1000);
  }, []);

  useEffect(() => {
    const source = getBoard();
    return () => source.cancel();
  }, [getBoard]);

  if (!board) return null;

  return (
    <Content style={{ margin: '0 16px' }}>
      <Breadcrumb style={{ margin: '16px 0' }}>
        <Breadcrumb.Item>Boards</Breadcrumb.Item>
        <Breadcrumb.Item>{board.name}</Breadcrumb.Item>
      </Breadcrumb>
      <PageHeader
        ghost={false}
        onBack={() => history.push('/')}
        title={board.name}
        subTitle={`Lists: ${board.lists.length}`}
        extra={[
          <Button key="1" type="primary" onClick={handleAddList}>
            <PlusOutlined />
            {' '}
            List
          </Button>,
          <Button key="2" danger onClick={handleRemove}>
            <DeleteOutlined />
            {' '}
            Board
          </Button>,
        ]}
      />
      <div className="site-layout-background" style={{ padding: 24 }}>
        <Row>
          {board.lists.map((list) => (
            <Col key={list.id} xs={24} sm={12} md={12} lg={12} xl={12} style={{ padding: 8 }}>
              <Card
                title={list.name}
                bordered={false}
                extra={[
                  <Button key="1" type="primary" onClick={handleAddCard.bind(this, list.id)} style={{ marginRight: 12 }}><PlusOutlined /></Button>,
                  <Button key="2" danger onClick={handleRemoveList.bind(this, list.id)}><DeleteOutlined /></Button>,
                ]}
              >
                {list.cards.map((card) => (
                  <Card
                    key={card.id}
                    title={card.name}
                    style={{ marginBottom: 24 }}
                    hoverable
                    extra={(
                      <Button danger onClick={handleRemoveCard.bind(this, card.id)}>
                        <DeleteOutlined />
                      </Button>
                    )}
                  >
                    <p>
                      <strong>Description</strong>
                      :
                      {' '}
                      {card.description}
                    </p>
                    <p>
                      <strong>Prio</strong>
                      :
                      {' '}
                      {card.prio}
                    </p>
                    <p>
                      <strong>Status</strong>
                      :
                    </p>
                    <Select id={`card_${card.id}`} onSelect={handleSelect.bind(this, card.id)} defaultValue={card.status} style={{ width: '100%' }}>
                      <Option value="Backlog">Backlog</Option>
                      <Option value="ToDo">ToDo</Option>
                      <Option value="In Progress">In Progress</Option>
                      <Option value="Testing">Testing</Option>
                      <Option value="Done">Done</Option>
                    </Select>
                  </Card>
                ))}
              </Card>
            </Col>
          ))}
        </Row>
      </div>
    </Content>
  );
}

Board.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  history: PropTypes.object.isRequired,
};

export default Board;
