# frozen_string_literal: true
module Api
  module V1
    class CardsController < ApplicationController
      before_action :set_card, only: %i[update destroy]

      def create
        @card = Card.new(card_params)
        success = @card.save
        render(json: { success: success })
      end

      def update
        success = @card.update(card_params)
        render(json: { success: success })
      end

      def destroy
        @card.destroy

        render(json: { success: true })
      end

      private

      def set_card
        @card = Card.find(params[:id])
      end

      def card_params
        params.require(:card).permit(:name, :description, :prio, :status, :list_id)
      end
    end
  end
end
