# frozen_string_literal: true
module Api
  module V1
    class BoardsController < ApplicationController
      before_action :set_board, only: %i[show destroy]

      def index
        render(json: Board.all)
      end

      def show
        render(json: @board)
      end

      def create
        @board = Board.new(board_params)
        success = @board.save
        render(json: { success: success })
      end

      def destroy
        @board.destroy

        render(json: { success: true })
      end

      private

      def set_board
        @board = Board.find(params[:id])
      end

      def board_params
        params.require(:board).permit(:name)
      end
    end
  end
end
