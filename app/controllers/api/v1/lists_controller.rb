# frozen_string_literal: true
module Api
  module V1
    class ListsController < ApplicationController
      before_action :set_list, only: %i[destroy]

      def create
        @list = List.new(list_params)
        success = @list.save
        render(json: { success: success })
      end

      def destroy
        @list.destroy

        render(json: { success: true })
      end

      private

      def set_list
        @list = List.find(params[:id])
      end

      def list_params
        params.require(:list).permit(:name, :board_id)
      end
    end
  end
end
