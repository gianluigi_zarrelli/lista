# Lista

* Ruby version
2.7.1

* System dependencies
1. React (Frontend)
2. antd (Frontend Framework)
3. Rails 6.0.3.1 (Backend)
4. Postgresql (Database)

* School
1. ER -> /school/er-diagram.png
2. SQL -> /school/sql.txt