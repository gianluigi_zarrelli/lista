# frozen_string_literal: true
25.times do
  board = Board.create(name: Faker::Company.name)

  5.times do |i|
    list = board.lists.create(name: "List #{i}")

    10.times do
      list.cards.create(name: Faker::App.name, description: Faker::Lorem.paragraph, prio: rand(1..5), status: 'Backlog')
    end
  end
end
